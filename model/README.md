# The role of R1-R4 interaction and nuclear shuttling for cell division control.

This folder holds the scripts used for modelling the nuclear shuttling together with plotting some of the quantified data. The model equations and choice of assumptions and parameter values are described in the Supplemental Information. 

## Reproducing modelling figure (fig. S14) 

Dowload and install [Julia](www.julialang.org). The code was written for Julia 1.4.2 but will likely work fine for any julia 1.x. 

Open and a Julia session and activate the Pkg environment of this package.
At leas on Unix systems, this is easiest done by navigating a terminal to the procjects root directory and start a julia REPL session with 
```
julia --project
```

If this is not available to you, just open the REPL and activate the project environment by

```julia
using Pkg
Pkg.activate("path/to/the/package/R1R4.jl")
```


Once the envionment is activated, instantiate it to get all of the dependencies.
```julia
using Pkg
Pkg.instantiate()
```

Now everything should be installed just like it was on the machine where we ran the code. 

The figures are generated in the `scripts/generate_figures.jl` file. It is runnable as is but if you want to change from deterministic to stochastic simulations you need to change a line in the code (see the script file). You also need to actually use the right package environment. Again, the easiest way to run it on should be to navigate your terminal to the project folder and execute
```julia
julia --project scripts/generate_figures.jl
```

You can also just keep a REPL open and copy chunks of the script to it for a more interactive experience. 

